from __future__ import absolute_import, unicode_literals

import unittest

from ecs.components.general import Position
from ecs.util.pathfinding import get_bounding_box


class TestGetBoundingBox(unittest.TestCase):
    def test_get_bounding_box(self):
        pos1 = Position(4, 4)
        pos2 = Position(7, 7)
        top_left, bottom_right = get_bounding_box(pos1, pos2, 2, 10, 10)
        assert top_left == Position(2, 2)
        assert bottom_right == Position(9, 9)

        pos1 = Position(7, 7)
        pos2 = Position(4, 4)
        top_left, bottom_right = get_bounding_box(pos1, pos2, 2, 10, 10)
        assert top_left == Position(2, 2)
        assert bottom_right == Position(9, 9)

        pos1 = Position(1, 3)
        pos2 = Position(4, 7)
        top_left, bottom_right = get_bounding_box(pos1, pos2, 2, 10, 10)
        assert top_left == Position(0, 1)
        assert bottom_right == Position(6, 9)

        pos1 = Position(5, 4)
        pos2 = Position(2, 3)
        top_left, bottom_right = get_bounding_box(pos1, pos2, 2, 10, 10)
        assert top_left == Position(0, 1)
        assert bottom_right == Position(7, 6)

        pos1 = Position(6, 4)
        pos2 = Position(6, 6)
        top_left, bottom_right = get_bounding_box(pos1, pos2, 2, 10, 10)
        assert top_left == Position(4, 2)
        assert bottom_right == Position(8, 8)

        pos1 = Position(6, 4)
        pos2 = Position(6, 6)
        top_left, bottom_right = get_bounding_box(pos1, pos2, 4, 7, 7)
        assert top_left == Position(2, 0)
        assert bottom_right == Position(7, 7)


class TestGetEntitiesAsGraph(unittest.TestCase):
    def test_get_entities_as_graph(self):
        entities = {
            1: {
                Position()
            }
        }



























