from __future__ import absolute_import, unicode_literals

import time

from ecs.components.general import Position
from ecs.entities.base import BaseEntity


class ECS(object):
    def __init__(self, entity_map):
        self.entities = {}
        self.components = {}
        self.processors = []
        self.current_id = 0

        self.entity_map = entity_map

    # -------- Processor Code
    def add_processor(self, processor, priority):
        # what to do about duplicates here?
        processor.ecs = self
        processor.priority = priority

        self.processors.append(processor)
        self.processors.sort(key=lambda p: p.priority)

    def process_all(self):
        for proc in self.processors:
            proc.process()

    # --------- Component Code
    def add_component(self, entity_id, component):
        c_type = type(component)
        if c_type not in self.components:
            self.components[c_type] = set()
        self.components[c_type].add(entity_id)
        self.entities[entity_id][c_type] = component

    # use lru caches?
    def get_matching_entity_ids(self, *c_types):
        try:
            return list(set.intersection(*[self.components[c] for c in c_types]))
        except KeyError:
            #print "No instances of:", c_types
            return []

    def remove_component(self, entity_id, c_type):
        self.components[c_type].discard(entity_id)
        if not self.components[c_type]:
            del self.components[c_type]
        print "REMOVING", entity_id
        del self.entities[entity_id][c_type]

    # --------- Entity Code
    def add_entity(self, *components):
        self.entities[self.current_id] = BaseEntity()
        for component in components:
            self.add_component(self.current_id, component)
            if type(component) == Position:
                self.entity_map.add_entity(self.entities[self.current_id], component)
        #print "added enitity", self.current_id, self.entities[self.current_id]
        self.current_id += 1

    def get_entity(self, entity_id):
        return self.entities[entity_id]

    def get_entities(self, entity_ids):
        return {x: self.entities[x] for x in entity_ids}

    def get_matching_entities(self, *c_types):
        entity_ids = self.get_matching_entity_ids(*c_types)
        return self.get_entities(entity_ids)

    def remove_entity(self, entity_id):
        print "removing entity", entity_id
        for c_type in self.entities[entity_id]:
            self.components[c_type].discard(entity_id)

            if not self.components[c_type]:
                del self.components[c_type]

        self.entity_map.remove_entity(self.entities[entity_id])
        del self.entities[entity_id]

    # --------- Map Code
    def move_entity(self, entity, pos):
        self.entity_map.move_entity(entity, pos)
























