from __future__ import absolute_import, unicode_literals

import ecs.components.general as general_c
import ecs.components.unit as unit_c
import ecs.components.tile as tile_c
from ecs.items.ranged import Crossbow


def dwarf_melee(x, y):
    components = (
        general_c.Image("[U+E106]"),
        general_c.Position(x, y),
        unit_c.Stats("dwarf", 250, 1, 30),
        unit_c.Walking(),
        unit_c.Melee([])
    )
    return components


def dwarf_ranged(x, y):
    components = (
        general_c.Image("[U+E106]"),
        general_c.Position(x, y),
        unit_c.Stats("dwarf", 150, 1, 30),
        unit_c.Walking(),
        unit_c.Ranged([Crossbow()])
    )
    return components

