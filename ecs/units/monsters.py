from __future__ import absolute_import, unicode_literals

import ecs.components.general as general_c
import ecs.components.unit as unit_c
import ecs.components.tile as tile_c


def slime(x, y):
    components = (
        general_c.Image("[U+E104]"),
        general_c.Position(x, y),
        unit_c.Stats("slime", 100, 2, 20),
        unit_c.Walking(),
        unit_c.Melee([])
    )
    return components
