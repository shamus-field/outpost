from __future__ import absolute_import, unicode_literals

import math

from bearlibterminal import terminal as ter

from ecs.processors.base import BaseProcessor
import ecs.components.general as general_c
import ecs.components.unit as unit_c


class Render(BaseProcessor):
    def __init__(self, map_size, health_pcts):
        self.map_size = map_size
        self.health_pcts = health_pcts
        self.num_health = len(health_pcts) - 1  # don't count 0%

    def process(self, v_offset, h_offset):
        screen_width = ter.state(ter.TK_WIDTH) * ter.state(ter.TK_CELL_WIDTH)
        screen_height = ter.state(ter.TK_HEIGHT) * ter.state(ter.TK_CELL_HEIGHT)
        tx = h_offset % 32
        ty = v_offset % 32

        ix = (h_offset - tx) / 32
        iy = (v_offset - ty) / 32
        print ix, iy
        if ix < 0:
            jx = (ix * -1) % self.map_size
        else:
            jx = self.map_size - (ix % self.map_size)

        if iy < 0:
            jy = (iy * -1) % self.map_size
        else:
            jy = self.map_size - (iy % self.map_size)

        hc = math.ceil((screen_width + 32 - tx) / float(32))
        vc = math.ceil((screen_height + 32 - ty) / float(32))

        for y in range(0, int(vc)):
            for x in range(0, int(hc)):
                mx = (jx + x) % self.map_size
                my = (jy + y) % self.map_size

                objs = self.ecs.entity_map.map_entities[mx][my]
                for obj in objs:
                    if not obj.get(general_c.Image):
                        continue

                    print_str = "[offset={},{}]{}".format(
                        (x - 1) * 32 + tx, (y - 1) * 32 + ty, obj[general_c.Image].image)
                    ter.print_(0, 0, print_str)

                    print_str = "[offset={},{}]{},{}".format(
                        (x - 1) * 32 + tx, (y - 1) * 32 + ty, obj[general_c.Position].x, obj[general_c.Position].y)
                    ter.print_(0, 0, print_str)



                    # if it has health, add the health bar
                    if obj.get(unit_c.Stats):
                        health = (obj[unit_c.Stats].hp / float(obj[unit_c.Stats].max_hp)) * 100.0
                        as_pct = int(math.ceil(health / float(self.num_health)) * self.num_health)

                        print_str = "[offset={},{}]{}".format(
                            (x - 1) * 32 + tx, (y - 1) * 32 + ty, self.health_pcts[as_pct])
                        ter.print_(0, 0, print_str)

































