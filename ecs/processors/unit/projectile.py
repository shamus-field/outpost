from __future__ import absolute_import, unicode_literals

import math

import ecs.components.general as general_c
import ecs.components.projectile as projectile_c
from ecs.processors.base import BaseProcessor


class Projectile(BaseProcessor):
    def __init__(self, projectiles):
        self.projectiles = projectiles

    def process(self):
        projectiles = self.ecs.get_matching_entities(projectile_c.LineProjectile)
        for e_id, entity in projectiles.items():
            current_pos = entity[general_c.Position]
            target_pos = entity[projectile_c.LineProjectile].target_pos
            source_pos = entity[projectile_c.LineProjectile].source_pos
            print source_pos, target_pos, "POS"
            if current_pos.x == target_pos.x and current_pos.y == target_pos.y:
                self.ecs.remove_entity(e_id)
                continue

            line = self.get_line(current_pos, target_pos)
            new_pos = general_c.Position(line[1][0], line[1][1])
            self.ecs.move_entity(entity, new_pos)
            entity[general_c.Position] = new_pos


            if not entity.get(general_c.Image):
                proj_type = entity[projectile_c.LineProjectile].p_type
                # if my projectile has multiple facing images, pick the one at the correct angle
                # TODO make it prettier by using offsets to make the angle better?
                if proj_type in self.projectiles:
                    dx = target_pos.x - source_pos.x
                    dy = target_pos.y - source_pos.y
                    angle = int(math.degrees(math.atan2(dy, dx)))
                    angle2 = math.atan2(dy, dx) * 180.0 / math.pi

                    # I think because the circle is shifted or something. I actually don't know. I was drunk when
                    # I wrote this code
                    if angle < 0:
                        angle = abs(angle)
                    elif angle >= 0:
                        angle = 360 - angle
                    print angle

                    closest = min(self.projectiles[proj_type], key=lambda x: abs(x - angle))
                    self.ecs.add_component(e_id, general_c.Image(self.projectiles[proj_type][closest]))
                # TODO if the projectile is a single image, not a list


    def get_line(self, start, end):
        """Bresenham's Line Algorithm
        Produces a list of tuples from start and end

        >>> points1 = get_line((0, 0), (3, 4))
        >>> points2 = get_line((3, 4), (0, 0))
        >>> assert(set(points1) == set(points2))
        >>> print points1
        [(0, 0), (1, 1), (1, 2), (2, 3), (3, 4)]
        >>> print points2
        [(3, 4), (2, 3), (1, 2), (1, 1), (0, 0)]
        """
        # Setup initial conditions
        x1 = start.x
        y1 = start.y
        x2 = end.x
        y2 = end.y
        dx = x2 - x1
        dy = y2 - y1

        # Determine how steep the line is
        is_steep = abs(dy) > abs(dx)

        # Rotate line
        if is_steep:
            x1, y1 = y1, x1
            x2, y2 = y2, x2

        # Swap start and end points if necessary and store swap state
        swapped = False
        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1
            swapped = True

        # Recalculate differentials
        dx = x2 - x1
        dy = y2 - y1

        # Calculate error
        error = int(dx / 2.0)
        ystep = 1 if y1 < y2 else -1

        # Iterate over bounding box generating points between start and end
        y = y1
        points = []
        for x in range(x1, x2 + 1):
            coord = (y, x) if is_steep else (x, y)
            points.append(coord)
            error -= abs(dy)
            if error < 0:
                y += ystep
                error += dx

        # Reverse the list if the coordinates were swapped
        if swapped:
            points.reverse()
        return points
