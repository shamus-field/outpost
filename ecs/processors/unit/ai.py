from __future__ import absolute_import, unicode_literals

import random
import time
from itertools import product

from ecs.processors.base import BaseProcessor
import ecs.components.general as general_c
import ecs.components.unit as unit_c
import ecs.components.projectile as projectile_c
import ecs.components.tile as tile_c
from ecs.util.pathfinding import make_graph, AStarGrid, are_pos_equal, get_bounding_box


class AI(BaseProcessor):
    def __init__(self, map_size):
        self.map_size = map_size
        self.slain = []

    def peace_think(self):
        pass

    def combat_think(self):
        pass

    def scan_for_enemies(self, entity, possible_targets):
        enemies = {}
        e_pos = entity[general_c.Position]
        e_stats = entity[unit_c.Stats]
        for p_id, p_target in possible_targets.items():
            # don't need to check it the p_target is the entity because the entity will always be on its own team
            if e_stats.team == p_target[unit_c.Stats].team:
                continue

            target_pos = p_target[general_c.Position]
            abs_x = abs(target_pos.x - e_pos.x)
            abs_y = abs(target_pos.y - e_pos.y)

            if abs_x <= e_stats.sight_range and abs_y <= e_stats.sight_range:
                enemies[p_id] = abs_x + abs_y

        dist_sort = [x[0] for x in sorted(enemies.items(), key=lambda v: v[1])]
        return dist_sort[0] if dist_sort else None

    def find_path(self):
        pass

    def move(self, entity, pos):
        self.ecs.move_entity(entity, pos)
        entity[general_c.Position].x = pos.x
        entity[general_c.Position].y = pos.y

    def process(self):
        entities = self.ecs.get_matching_entities(unit_c.Stats, unit_c.Walking, general_c.Position)

        for e_id, entity in entities.items():
            if e_id in self.slain:
                continue

            # unit not ready to move yet, so wait
            if entity[unit_c.Stats].ticks > 0:
                entity[unit_c.Stats].ticks -= 1
                continue
            else:
                entity[unit_c.Stats].ticks = entity[unit_c.Stats].speed

            print e_id, entity[unit_c.Stats].name, entity, entity[general_c.Position]
            base_target = entity.get(unit_c.Target)
            target = None
            if base_target:
                print "has base target"
                if entity[unit_c.Target].entity_id in self.slain:
                    self.ecs.remove_component(e_id, unit_c.Target)
                    print "target slain"
                else:
                    print "checking positions"
                    target = entities[entity[unit_c.Target].entity_id]
                    orig_target_pos = entity[unit_c.Target].orig_entity_pos
                    current_target_pos = target[general_c.Position]
                    print "comparing positions", orig_target_pos, current_target_pos
                    if not are_pos_equal(orig_target_pos, current_target_pos):
                        print "positions not equal"
                        target = None

            if not target:
                possible_targets = self.ecs.get_matching_entities(unit_c.Stats, general_c.Position)
                target_id = self.scan_for_enemies(entity, possible_targets)
                if target_id:
                    target_entity = self.ecs.get_entity(target_id)
                    target_pos = general_c.Position(target_entity[general_c.Position].x,
                                                    target_entity[general_c.Position].y)  # because immutable
                    self.ecs.add_component(e_id, unit_c.Target(target_id, target_pos))
                    target = possible_targets[target_id]

            if not target:
                # ok it's peace time, pick a random goal
                continue

            possible_actions = {}
            if entity.get(unit_c.Melee):
                for i, j in product([-1, 0, 1], [-1, 0, 1]):
                    check_x = entity[general_c.Position].x + i
                    check_y = entity[general_c.Position].y + j

                    if target[general_c.Position].x == check_x and target[general_c.Position].y == check_y:
                        possible_actions["attack_melee"] = entity[unit_c.Melee].weapons
                        break

                if "attack_melee" not in possible_actions:
                    possible_actions["move"] = []  # allow possible move abilities

            if entity.get(unit_c.Ranged):
                for weapon in entity[unit_c.Ranged].weapons:
                    abs_x = abs(entity[general_c.Position].x - target[general_c.Position].x)
                    abs_y = abs(entity[general_c.Position].y - target[general_c.Position].y)
                    if abs_x <= weapon.max_range and abs_y <= weapon.max_range:
                        if "attack_ranged" not in possible_actions:
                            possible_actions["attack_ranged"] = []
                        possible_actions["attack_ranged"].append(weapon)

                if "attack_ranged" not in possible_actions:
                    possible_actions["move"] = []

            # spells

            # abilities

            # how to distinguish good from bad for the two above?

            # could weight for some things higher than others, just do random for now
            action = random.choice(possible_actions.keys())
            print action
            if action == "move":
                """
                1) If entity already has a move, verify the target hasn't moved, and the move is valid
                2) If failure, and I'm far away from the target, just try to move one tile in their direction
                3) If failure, make a small box around source/target and try a pathfind
                4) If failure, make a large box around source/target and try a pathfind

                Should I check if an enemy is closer than my original target each time?
                """
                orig_target_pos = entity[unit_c.Target].orig_entity_pos
                current_target_pos = target[general_c.Position]
                entity_pos = entity[general_c.Position]
                target_path = entity[unit_c.Target].path
                print entity_pos, current_target_pos

                moved = False
                # check # 1
                if target_path and are_pos_equal(orig_target_pos, current_target_pos):
                    print "has_path"
                    move = target_path.pop(0)
                    print move
                    move_entities = self.ecs.entity_map.map_entities[move.x][move.y]
                    p_passable = set(entity[unit_c.Stats].passable_terrain)
                    failure = False
                    for me in move_entities:
                        if not me.get(tile_c.Passable) or not p_passable.intersection(set(me[tile_c.Passable].types)):
                            failure = True
                            break

                    if not failure:
                        self.move(entity, move)
                        moved = True

                # check # 2
                if not moved:
                    abs_x = abs(current_target_pos.x - entity_pos.x)
                    abs_y = abs(current_target_pos.y - entity_pos.y)
                    target_range = abs_x + abs_y
                    if target_range > 10:
                        shortest_abs = 999
                        shortest_x = 999
                        shortest_y = 999
                        for i, j in product([-1, 0, 1], [-1, 0, 1]):
                            check_x = entity[general_c.Position].x + i
                            check_y = entity[general_c.Position].y + j
                            check_abs = abs(check_x - entity_pos.x) + abs(check_y - entity_pos.y)
                            if check_abs < shortest_abs:
                                shortest_abs = check_abs
                                shortest_x = check_x
                                shortest_y = check_y

                        move_entities = self.ecs.entity_map.map_entities[shortest_x][shortest_y]
                        p_passable = set(entity[unit_c.Stats].passable_terrain)
                        failure = False
                        for me in move_entities:
                            if not me.get(tile_c.Passable) or not p_passable.intersection(set(me[tile_c.Passable].types)):
                                failure = True
                                break

                        if not failure:
                            self.move(entity, general_c.Position(shortest_x, shortest_y))
                            moved = True

                # check # 3
                if not moved:
                    bounding_box = get_bounding_box(entity_pos, current_target_pos, 2,
                                                    self.ecs.entity_map.map_size, self.ecs.entity_map.map_size)
                    print bounding_box, "BOUNDS"
                    if bounding_box[1].x - bounding_box[0].x >= bounding_box[1].y - bounding_box[0].y:
                        small_map_size = bounding_box[1].x - bounding_box[0].x
                    else:
                        small_map_size = bounding_box[1].y - bounding_box[0].y

                    graph, nodes = make_graph(small_map_size, entity, target, self.ecs.entity_map.map_entities,
                                              bounding_box[0])
                    paths = AStarGrid(graph)
                    entity_offset_x = entity[general_c.Position].x - bounding_box[0].x
                    entity_offset_y = entity[general_c.Position].y - bounding_box[0].y
                    target_offset_x = target[general_c.Position].x - bounding_box[0].x
                    target_offset_y = target[general_c.Position].y - bounding_box[0].y

                    path = paths.search(nodes[entity_offset_x][entity_offset_y],
                                        nodes[target_offset_x][target_offset_y])
                    if path:
                        new_path = [general_c.Position(z.x + bounding_box[0].x, z.y + bounding_box[0].y) for z in path]
                        new_path.pop(0)
                        next_move = new_path.pop(0)
                        entity[unit_c.Target].path = new_path
                        self.move(entity, general_c.Position(next_move.x, next_move.y))
                        moved = True

                #assert 0
                if not moved:
                    # check # 4
                    graph, nodes = make_graph(self.map_size, entity, target, self.ecs.entity_map.map_entities,
                                              general_c.Position(0, 0))
                    paths = AStarGrid(graph)
                    path = paths.search(nodes[entity[general_c.Position].x][entity[general_c.Position].y],
                                        nodes[target[general_c.Position].x][target[general_c.Position].y])
                    if path:
                        path.pop(0)
                        next_move = path.pop(0)
                        entity[unit_c.Target].path = path
                        self.move(entity, general_c.Position(next_move.x, next_move.y))
            elif action == "attack_melee":
                weapon = random.choice(possible_actions["attack_melee"])
                target[unit_c.Stats].hp -= weapon.damage

                # apply any effects from the weapon
            elif action == "attack_ranged":
                weapon = random.choice(possible_actions["attack_ranged"])
                target[unit_c.Stats].hp -= weapon.damage

                self.ecs.add_entity(
                    projectile_c.LineProjectile(50, "bolt", entity[general_c.Position], target[general_c.Position]),
                    general_c.Position(entity[general_c.Position].x, entity[general_c.Position].y))

                # apply any effects from the weapon


            # clean up if the target died
            if target[unit_c.Stats].hp <= 0:
                self.slain.append(entity[unit_c.Target].entity_id)
                self.ecs.remove_entity(entity[unit_c.Target].entity_id)

























