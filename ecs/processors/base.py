from __future__ import absolute_import, unicode_literals


class BaseProcessor(object):
    def __init__(self, priority=999):
        self.ecs = None
        self.priority = priority

    def process(self, *args):
        pass
