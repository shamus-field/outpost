from __future__ import absolute_import, unicode_literals


class Melee(object):
    pass


class Fists(Melee):
    def __init__(self, damage=10):
        self.damage = damage
