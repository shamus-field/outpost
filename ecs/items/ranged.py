from __future__ import absolute_import, unicode_literals


class Ranged(object):
    pass


class Crossbow(Ranged):
    def __init__(self, damage=10, max_range=4, projectile="bolt"):
        self.damage = damage
        self.max_range = max_range
        self.projectile = projectile
