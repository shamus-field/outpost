from __future__ import absolute_import, unicode_literals


class LineProjectile(object):
    def __init__(self, speed, p_type, source_pos, target_pos):
        self.speed = speed
        self.p_type = p_type
        self.source_pos = source_pos
        self.target_pos = target_pos
