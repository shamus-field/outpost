from __future__ import absolute_import, unicode_literals


class Passable(object):
    def __init__(self, types):
        self.types = types


class NotPassable(object):
    def __init__(self):
        pass
