from __future__ import absolute_import, unicode_literals

import random

from ecs.items.melee import Fists


class Stats(object):
    def __init__(self, name, max_hp, team, speed, dext=None, inte=None, stre=None):
        self.name = name
        self.sight_range = 6
        self.team = team
        self.passable_terrain = ["ground"]

        if not dext:
            self.dext = random.randint(0, 10)
        else:
            self.dext = dext

        if not inte:
            self.inte = random.randint(0, 10)
        else:
            self.inte = inte

        if not stre:
            self.stre = random.randint(0, 10)
        else:
            self.stre = stre

        self.ticks = speed - self.dext
        self.speed = speed - self.dext

        self.max_hp = max_hp + 2 * self.stre
        self.hp = max_hp + 2 * self.stre


class Target(object):
    def __init__(self, entity_id, orig_entity_pos):
        self.entity_id = entity_id
        self.orig_entity_pos = orig_entity_pos
        self.path = []


class Walking(object):
    def __init__(self):
        pass


class Melee(object):
    def __init__(self, weapons):
        self.weapons = weapons
        self.weapons.append(Fists())


class Ranged(object):
    def __init__(self, weapons):
        self.weapons = weapons
