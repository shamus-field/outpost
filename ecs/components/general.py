from __future__ import absolute_import, unicode_literals


class Position(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "Position({},{})".format(self.x, self.y)

    def __eq__(self, other):
        return isinstance(other, Position) and other.x == self.x and other.y == self.y


class Image(object):
    def __init__(self, image):
        self.image = image
