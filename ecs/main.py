from __future__ import absolute_import, unicode_literals

import os
import random

from bearlibterminal import terminal as ter

from ecs.ecs_main.ecs_main import ECS
from ecs.processors.general.render import Render
import ecs.components.general as general_c
import ecs.components.unit as unit_c
import ecs.components.tile as tile_c
from ecs.processors.unit.ai import AI
from ecs.processors.unit.projectile import Projectile
from ecs.items.ranged import Crossbow
from ecs.util.entity_map import EntityMap

from ecs.units.dwarves import dwarf_melee, dwarf_ranged
from ecs.units.monsters import slime


FPS = 30
SKIP_TICKS = 1000/FPS
TILE_SIZE_X = 4  # not really sure why icons take up four x but only two y....
TILE_SIZE_Y = 2
SCROLL_CAP = 25
MAP_SIZE = 64


## random todos
# TODO couple map move/unit move so one can't happen without the other
# TODO debug mode with entity ids printing on entities


def test_game(game_ecs):
    for x in range(0, MAP_SIZE):
        for y in range(0, MAP_SIZE):
            game_ecs.add_entity(general_c.Image("[U+E100]"), general_c.Position(x, y), tile_c.Passable(["ground"]))

    game_ecs.add_entity(*dwarf_ranged(0, 0))
    game_ecs.add_entity(*dwarf_ranged(1, 1))
    game_ecs.add_entity(*dwarf_ranged(8, 8))
    game_ecs.add_entity(*dwarf_ranged(2, 8))
    game_ecs.add_entity(*dwarf_ranged(7, 3))
    game_ecs.add_entity(*dwarf_melee(2, 3))
    game_ecs.add_entity(*dwarf_melee(2, 2))

    game_ecs.add_entity(*slime(4, 3))
    game_ecs.add_entity(*slime(6, 6))
    game_ecs.add_entity(*slime(6, 7))
    game_ecs.add_entity(*slime(9, 8))
    game_ecs.add_entity(*slime(5, 8))
    game_ecs.add_entity(*slime(6, 8))
    game_ecs.add_entity(*slime(7, 8))

    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(3, 3))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(2, 4))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(3, 4))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(6, 3))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(7, 2))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(6, 2))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(8, 2))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(9, 2))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(10, 2))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(6, 4))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(7, 4))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(8, 4))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(9, 4))
    game_ecs.add_entity(
        general_c.Image("[U+E103]"), general_c.Position(10, 4))


class GameState(object):
    def __init__(self, ecs):
        self.ecs = ecs
        self.state = "main_menu"

    def run_game(self, health_pcts, projectiles):
        self.state = "running"
        h_speed = 0
        h_offset = 0
        v_speed = 0
        v_offset = 0
        random_seed = "".join([str(random.randint(0, 9)) for _ in range(6)])
        random_seed = "226586"
        print random_seed
        random.seed(random_seed)
        #print random.getstate()
        #print random.randint(0, 9)
        #assert 0

        # add any processors which are never recreated
        self.ecs.add_processor(AI(MAP_SIZE), 0)
        renderer = Render(MAP_SIZE, health_pcts)
        renderer.ecs = self.ecs

        self.ecs.add_processor(Projectile(projectiles), 1)

        # TEST CODE/DO FIRST RENDER
        test_game(self.ecs)
        renderer.process(0, 0)
        ter.refresh()

        import time
        while True:
            h_offset -= h_speed
            v_offset -= v_speed
            ter.clear()
            advance_round = False

            s = time.time()
            keys = []
            while ter.has_input():
                keys.append(ter.read())

            if ter.TK_ESCAPE in keys:
                break
            elif ter.TK_SPACE in keys:
                if self.state == "running":
                    self.state = "paused"
                else:
                    self.state = "running"
            elif ter.TK_R in keys:
                if self.state == "running":
                    self.state = "round_based"
                else:
                    self.state = "running"
            elif ter.TK_F in keys:
                advance_round = True

            if ter.state(ter.TK_LEFT) or ter.state(ter.TK_A):
                if h_speed > SCROLL_CAP * -1:
                    h_speed -= 1
            elif ter.state(ter.TK_RIGHT) or ter.state(ter.TK_D):
                if h_speed < SCROLL_CAP:
                    h_speed += 1
            else:
                h_speed -= ((0 < h_speed) - (h_speed < 0))

            if ter.state(ter.TK_UP) or ter.state(ter.TK_W):
                if v_speed > SCROLL_CAP * -1:
                    v_speed -= 1
            elif ter.state(ter.TK_DOWN) or ter.state(ter.TK_S):
                if v_speed < SCROLL_CAP:
                    v_speed += 1
            else:
                v_speed -= ((0 < v_speed) - (v_speed < 0))

            if self.state == "running":
                self.ecs.process_all()
            elif self.state == "round_based" and advance_round:
                print "round\n"
                self.ecs.process_all()
                advance_round = False

            renderer.process(v_offset, h_offset)
            ter.refresh()

            if self.state == "running":
                print time.time() - s, "\n\n"
            ter.delay(SKIP_TICKS)




if __name__ == "__main__":
    ter.open()
    ter.set("window: size=100x50")
    ter.composition(ter.TK_ON)
    ter.set("U+E100: ./images/grass/grass0.png, size=32x32, align=top-left")
    ter.set("U+E101: ./images/dngn_closed_door.png, size=32x32, align=top-left")
    ter.set("U+E102: ./images/acid_blob.png, size=32x32, align=top-left")
    ter.set("U+E103: ./images/tree1_lightred.png, size=32x32, align=top-left")
    ter.set("U+E104: ./images/giant_amoeba.png, size=32x32, align=top-left")
    ter.set("U+E105: ./images/brick_dark2.png, size=32x32, align=top-left")
    ter.set("U+E106: ./images/dwarf.png, size=32x32, align=top-left")
    ter.set("U+E107: ./images/i-heal-wounds.png, size=32x32, align=top-left")
    ter.set("U+E108: ./images/projectiles/bolt/bolt0.png, size=32x32, align=top-left")

    health_pcts = {}
    i = 800
    for f in os.listdir("./images/health_bars"):
        pct = int(f.split("_")[-1].split(".")[0])
        health_pcts[pct] = "[U+E{}]".format(i)
        ter.set("U+E{}: ./images/health_bars/{}, size=32x32, align=top-left".format(i, f))
        i += 1

    projectiles = {}
    i = 1000
    proj_dir = "./images/projectiles"
    for proj in os.listdir(proj_dir):
        if os.path.isdir(os.path.join(proj_dir, proj)):
            projectiles[proj] = {}
            for fi in os.listdir(os.path.join(proj_dir, proj)):
                degrees = int(fi.lstrip(proj).split(".")[0])
                projectiles[proj][degrees] = "[U+E{}]".format(i)
                ter.set("U+E{}: {}, size=32x32, align=top-left".format(i, os.path.join(proj_dir, proj, fi)))
                i += 1
            projectiles[proj][360] = projectiles[proj][0]

    for i in range(0, 11):
        start = 500
        ter.set("U+E{}".format(start + i))

    game = GameState(ECS(EntityMap(MAP_SIZE)))

    while True:
        ter.layer(0)
        ter.clear()
        ter.refresh()

        key = ter.read()
        if key == ter.TK_ESCAPE:
            break
        elif key == ter.TK_ENTER and game.state == "main_menu":
            print "CATCH BELOW"
            game.run_game(health_pcts, projectiles)

    ter.close()
