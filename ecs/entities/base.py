from __future__ import absolute_import, unicode_literals


class BaseEntity(dict):
    #def __repr__(self):
    #    ret = "{\n"
    #    for k, v in self.items():
    #        print k, v
    #        ret += ("    " + v.__class__.__name__ + "\n")
    #    return ret + "n\}"
    def __repr__(self):
        return "{" + ",".join([v.__class__.__name__ for k, v in self.items()]) + "}"
