from __future__ import absolute_import, unicode_literals

from ecs.components.general import Position


class EntityMap(object):
    def __init__(self, map_size):
        self.map_size = map_size
        self.map_entities = []
        for x in range(0, map_size):
            x_tiles = []
            for y in range(0, map_size):
                x_tiles.append([])
            self.map_entities.append(x_tiles)

    def add_entity(self, entity, pos):
        self.map_entities[pos.x][pos.y].append(entity)

    def move_entity(self, entity, new_pos):
        self.map_entities[entity[Position].x][entity[Position].y].remove(entity)
        self.map_entities[new_pos.x][new_pos.y].append(entity)

    def remove_entity(self, entity):
        self.map_entities[entity[Position].x][entity[Position].y].remove(entity)

    def get_entities(self, pos):
        return self.map_entities[pos.x][pos.y]




