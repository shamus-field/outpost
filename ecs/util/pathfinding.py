from __future__ import absolute_import, unicode_literals

from math import sqrt
from itertools import product
import time

import ecs.components.general as general_c
import ecs.components.tile as tile_c
import ecs.components.unit as unit_c


# GITHUB shamsHub7



class AStar(object):
    def __init__(self, graph):
        self.graph = graph

    def heuristic(self, node, start, end):
        raise NotImplementedError

    def search(self, start, end):
        print start.x, start.y, end.x, end.y
        openset = set()
        closedset = set()
        current = start
        openset.add(current)
        while openset:
            current = min(openset, key=lambda o: o.g + o.h)
            if current == end:
                path = []
                while current.parent:
                    path.append(current)
                    current = current.parent
                path.append(current)
                return path[::-1]
            openset.remove(current)
            closedset.add(current)
            for node in self.graph[current]:
                if node in closedset:
                    continue
                if node in openset:
                    new_g = current.g + current.move_cost(node)
                    if node.g > new_g:
                        node.g = new_g
                        node.parent = current
                else:
                    node.g = current.g + current.move_cost(node)
                    node.h = self.heuristic(node, start, end)
                    node.parent = current
                    openset.add(node)
        return None


class AStarNode(object):
    def __init__(self):
        self.g = 0
        self.h = 0
        self.parent = None

    def move_cost(self, other):
        raise NotImplementedError


class AStarGrid(AStar):
    def heuristic(self, node, start, end):
        return sqrt((end.x - node.x)**2 + (end.y - node.y)**2)


class AStarGridNode(AStarNode):
    def __init__(self, x, y):
        self.x, self.y = x, y
        super(AStarGridNode, self).__init__()

    def __repr__(self):
        return "AStarGridNode({},{})".format(self.x, self.y)

    def move_cost(self, other):
        diagonal = abs(self.x - other.x) == 1 and abs(self.y - other.y) == 1
        return 14 if diagonal else 10


def make_graph(map_size, pathing_entity, target_entity, entity_map, offset):
    p_passable = set(pathing_entity[unit_c.Stats].passable_terrain)
    print offset.x, offset.y, map_size
    pe_pos = pathing_entity[general_c.Position]
    ta_pos = target_entity[general_c.Position]

    nodes = [[AStarGridNode(x, y) for y in range(map_size)] for x in range(map_size)]
    graph = {}
    for x, y in product(range(map_size), range(map_size)):
        node = nodes[x][y]
        graph[node] = []
        for i, j in product([-1, 0, 1], [-1, 0, 1]):
            check_x = x + i
            check_y = y + j

            if not (0 <= check_x < map_size):
                continue
            if not (0 <= check_y < map_size):
                continue

            if (check_x == (pe_pos.x - offset.x) and check_y == (pe_pos.y - offset.y)) or \
                    (check_x == (ta_pos.x - offset.x) and check_y == (ta_pos.y - offset.y)):
                graph[nodes[x][y]].append(nodes[check_x][check_y])
                continue

            entities = entity_map[check_x + offset.x][check_y + offset.y]

            use_node = True
            for e in entities:
                if not e.get(tile_c.Passable) or not p_passable.intersection(set(e[tile_c.Passable].types)):
                    use_node = False
                    break

            if use_node:
                graph[nodes[x][y]].append(nodes[check_x][check_y])
    return graph, nodes


def entities_as_graph(entities, top_left, bottom_right):
    graph = {}
    for _, val in entities.items():
        pos_x = val[general_c.Position].x
        pos_y = val[general_c.Position].y

        if pos_x < top_left.x or pos_x > bottom_right.x:
            continue
        if pos_y < top_left.y or pos_y > bottom_right.y:
            continue

        transformed_x = pos_x - top_left.x
        transformed_y = pos_y - top_left.y
        if pos_x not in graph:
            graph[transformed_x] = {}
        if pos_y not in graph[pos_x]:
            graph[transformed_x][transformed_y] = []
        graph[transformed_x][transformed_y].append(val)
    return graph


def are_pos_equal(pos1, pos2):
    if pos1.x == pos2.x and pos1.y == pos2.y:
        return True
    return False


def get_bounding_box(pos1, pos2, edge, max_x, max_y):
    if pos1.x <= pos2.x:
        left_x = max((pos1.x - edge), 0)
        right_x = min((pos2.x + edge), max_x)
    else:
        left_x = max((pos2.x - edge), 0)
        right_x = min((pos1.x + edge), max_x)

    if pos1.y <= pos2.y:
        top_y = max((pos1.y - edge), 0)
        bottom_y = min((pos2.y + edge), max_y)
    else:
        top_y = max((pos2.y - edge), 0)
        bottom_y = min((pos1.y + edge), max_y)

    return general_c.Position(left_x, top_y), general_c.Position(right_x, bottom_y)

















